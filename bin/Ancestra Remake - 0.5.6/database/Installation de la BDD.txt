Cr�er deux bases de donn�es : 
ancestra_other
ancestra_static

*Comment cela fonctionne ?
Patch revYY - Commentaire _XXXX.sql
YY correspond au num�ro du patch.
XXXX correspond a la base de donn�es ou doit �tre appliquer le patch.


*Si vous ex�cuter les patchs :
- "ancestra_other_old.sql" dans la base de donn�es "ancestra_other".
- "ancestra_static_old.sql" dans la base de donn�es "ancestra_static".
Aller dans le r�pertoire Patchs et executer dans l'ordre croissant les patchs en fonction de leur attribuent.

*Si vous ex�cuter les patchs :
- "ancestra_other_Patched94.sql" dans la base de donn�es "ancestra_other".
- "ancestra_static_Patched94.sql" dans la base de donn�es "ancestra_static".
Aller dans le r�pertoire Patchs et executer dans l'ordre croissant les patchs sup�rieur au num�ro 94.


/!\ Attention, les r�visions des patchs ne correspondent pas aux r�visions de la version du core.